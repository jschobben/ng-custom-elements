import {Component, EventEmitter, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AlertComponent {
  private readonly onClose: EventEmitter<void> = new EventEmitter();

  @Input() accent: string;
  @Input() dismissible: boolean;

  onCloseClick(): void {
    this.onClose.emit();
  }
}
