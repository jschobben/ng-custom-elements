import { BrowserModule } from '@angular/platform-browser';
import {ApplicationRef, DoBootstrap, Injector, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {AlertComponent} from './alert/alert.component';
import {createCustomElement} from '@angular/elements';
import { ButtonComponent } from './button/button.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    ButtonComponent
  ],
  imports: [
    BrowserModule
  ],
})
export class AppModule implements DoBootstrap {
  constructor(private injector: Injector) {}
  ngDoBootstrap(appRef: ApplicationRef): void {
    const alertEl = createCustomElement(AlertComponent, { injector: this.injector});
    const buttonEl = createCustomElement(ButtonComponent, { injector: this.injector});
    customElements.define('regas-alert', alertEl);
    customElements.define('regas-button', buttonEl);
  }

}
